#Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.
tsk1 = '-------------------------------------------- Task 1 ---------------------------------------------------------'
space = '-------------------------------------------------------------------------------------------------------------'
print(space)
print(tsk1)
print(space)
vowels = 'aeiouAEIOU'  #літери повторюються, тому що програма повинна виконуватись на двох мовах, тому і літери з англійської та української мов
str = input('Введіть будь ласка своє речення.\n')
print(space)
sentence = str.split()
count = 0
for word in sentence:
    checker = False
    double_vowels = 0
    for symbol in word:
        if symbol in vowels:
            double_vowels += 1
            if double_vowels == 2:
                checker = True
                break
        else:
            double_vowels = 0

    if checker:
        count += 1
print('Кількість слів з двома голосними літерами підряд ---> ', count)
print(space)


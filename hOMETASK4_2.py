#Є два довільних числа які відповідають за мінімальну і максимальну ціну. Є Dict з назвами магазинів і цінами:
# { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}.
# #Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною.
tsk2 = '-------------------------------------------- Task 2 ---------------------------------------------------------'
space = '-------------------------------------------------------------------------------------------------------------'
print(space)
print(tsk2)
print(space)
catalog = {"cito": 47.999,
           "BB_studio": 42.999,
           "momo": 49.999,
           "main-service": 37.245,
           "buy.now": 38.324,
           "x-store": 37.166,
           "the_partner": 38.988,
           "store": 37.720,
           "rozetka": 38.003}
value1 = input('Введть будь ласка мінімальну ціну.\n')
print(space)
value2 = input('Введть будь ласка максимальну ціну.\n')
print(space)
if value1.isdigit() and value2.isdigit() or '.' in value1 or '.' in value2:
    min = float(value1)
    max = float(value2)
else:
    print('ERROR!')
    exit()
if min >= 0 and max >=0 and max > min:
    pass
else:
    print('ERROR!')
    exit()
for key, value in catalog.items():
    if value <= max and value >= min:
        print(key)
        print(space)
        continue
    else:
        pass
if max < 37.166:
    print('За заданим діапазоном цін нічого не знайдено!')
    print(space)